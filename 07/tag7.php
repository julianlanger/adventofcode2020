<?php declare(strict_types=1);
$input = file(__DIR__ . '/input');

$bags = [];

foreach ($input as $data) {
    $dataArr  = explode(' ', $data);
    $color    = $dataArr[0] . ' ' . $dataArr[1];
    $contains = [];
    foreach ($dataArr as $index => $value) {
        if (is_numeric($value)) {
            $contains[$dataArr[$index + 1] . ' ' . $dataArr[$index + 2]] = (int)$value;
        }
    }
    
    $bags[$color] = $contains;
}

function getAllShinyGoldBags(array $bags): int
{
    $count = 0;
    foreach (array_keys($bags) as $bagType) {
        if (canContain($bags, $bagType)) {
            $count++;
        }
    }
    return $count;
}


function canContain(array $bags, string $bagType): bool
{
    if (isset($bags[$bagType]['shiny gold'])) {
        return true;
    }
    foreach (array_keys($bags[$bagType]) as $child) {
        if (canContain($bags, $child)) {
            return true;
        }
    }
        return false;
}

print_r('Part1: ' . getAllShinyGoldBags($bags) . PHP_EOL);

//part2:

function getBagAmount(array $bags, string $bagType): int
{
    $sum = 1;
    foreach ($bags[$bagType] as $bag => $amount) {
        $sum += getBagAmount($bags, $bag) * $amount;
    }
    
    return $sum;
}

print_r('Part2: ' . (getBagAmount($bags, 'shiny gold') - 1) . PHP_EOL);
