<?php declare(strict_types=1);

$input = file(__DIR__ . '/input');

foreach ($input as $line => $command) {
    [$command, $amount] = explode(' ', $command);
    $input[$line]       = ['command' => $command, 'amount' => (int)$amount];
}

function run($input): array
{
    $instructionIndex = 0;
    $executed         = [];
    $acc              = 0;
    while (!in_array($instructionIndex, $executed) && $instructionIndex < count($input)) {
        $command    = $input[$instructionIndex]['command'];
        $amount     = $input[$instructionIndex]['amount'];
        $executed[] = $instructionIndex;
        switch ($command) {
            case 'jmp':
                $instructionIndex += $amount;
                break;
            case 'acc':
                $acc += $amount;
                $instructionIndex++;
                break;
            default:
                $instructionIndex++;
                break;
        }
    }
    return(
        [
            'instructionIndex' => $instructionIndex,
            'accumulator' => $acc,
            'stuck' => $instructionIndex !== count($input)
        ]
    );
}

//part1:
print_r('Part1: ' . changeRow($input)['accumulator'] . PHP_EOL);

//part2:

function modifyInput(array $input, int $index): array
{
    $input[$index]['command'] = $input[$index]['command'] === 'jmp' ? 'nop' : 'jmp';
    
    return $input;
}

function testInput(array $input): int
{
    $run = changeRow($input);
    if ($run['stuck'] === false) {
        return $run['accumulator'];
    }
    $currentLine = 0;
    while ($currentLine < count($input)) {
        if (in_array($input[$currentLine]['command'], ['nop', 'jmp'])) {
            $run = changeRow(modifyInput($input, $currentLine));
            if ($run['stuck'] === false) {
                return $run['accumulator'];
            }
        }
        $currentLine++;
    }
}


print_r('Part2: ' . testInput($input) . PHP_EOL);
