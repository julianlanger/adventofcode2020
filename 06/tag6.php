<?php declare(strict_types=1);

$input = explode(PHP_EOL . PHP_EOL, file_get_contents(__DIR__ . '/input'));

foreach ($input as $index => $data) {
    $answers      = explode(PHP_EOL, $data);
    $groupAnswers = '';
    $groupMembers = 0;
    foreach ($answers as $answer) {
        $groupAnswers .= $answer;
        $groupMembers++;
    }
    $input[$index] = ['answers' => $groupAnswers, 'members' => $groupMembers];
}

//Part1:

function getSumAnyone(array $input): int
{
    $sum = 0;
    foreach ($input as $groupAnswer) {
        $answersInGroup = array_unique(str_split($groupAnswer['answers']));
        $sum           += count($answersInGroup);
    }
    
    return $sum;
}

print_r('Part1 sum of anyone\'s answers: ' . getSumAnyone($input) . PHP_EOL);

//Part2:

function getSumEveryone(array $input): int
{
    $sum = 0;
    foreach ($input as $groupAnswers) {
        $answers    = $groupAnswers['answers'];
        $answerTemp = [];
        foreach (str_split($answers) as $answer) {
            if (strlen(preg_replace("/[^$answer]/i", '', $answers)) === $groupAnswers['members'] &&
                !in_array($answer, $answerTemp)
            ) {
                $answerTemp[] = $answer;
            }
        }
        $sum += count($answerTemp);
    }
    
    return $sum;
}

print_r('Part2 sum of everyone\'s answers: ' . getSumEveryone($input) . PHP_EOL);
