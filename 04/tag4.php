<?php declare(strict_types=1);

$input = explode("\n", file_get_contents(__DIR__ . '/input'));

$passports      = [];
$passport       = '';
$line           = 0;
$passportsIndex = 0;
while ($line < count($input)) {
    if (strlen($input[$line]) > 0) {
        $passport .= $input[$line];
        if (isset($input[$line + 1]) && strlen($input[$line + 1]) > 0) {
            $passport .= ' ';
        }
    }
    if (!$input[$line] || $line === count($input) - 1) {
        $passports[$passportsIndex] = explode(' ', $passport);
        foreach ($passports[$passportsIndex] as $index => $data) {
            $tmpArr                                 = explode(':', $data);
            $passports[$passportsIndex][$tmpArr[0]] = $tmpArr[1];
            unset($passports[$passportsIndex][$index]);
        }
        $passport = '';
        $passportsIndex++;
    }
    $line++;
}

function isValid(array $passport, array $neededFields, array $validationRules = []): bool
{
    foreach ($neededFields as $field) {
        if (!isset($passport[$field])) {
            return false;
        } elseif (isset($validationRules[$field])) {
            if (!preg_match($validationRules[$field], $passport[$field])) {
                return false;
            }
        }
    }
    return true;
}

function countValidPassports(array $passports, array $neededFields, array $validationRules = []): int
{
    $validPassports = 0;
    foreach ($passports as $passport) {
        if (isValid($passport, $neededFields, $validationRules)) {
            $validPassports++;
        }
    }
    
    return $validPassports;
}

$validFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'];
//part1:
print_r('Part1: ' . countValidPassports($passports, $validFields) . PHP_EOL);

//part2:
$validationRules = [
    'byr' => '/^(19[2-8][0-9]|199[0-9]|200[0-2])$/m',
    'iyr' => '/^(201[0-9]|2020)$/m',
    'eyr' => '/^(202[0-9]|2030)$/m',
    'hgt' => '/(^(1[5-8][0-9]|19[0-3])cm$)|(^(59|6[0-9]|7[0-6])in$)/m',
    'hcl' => '/^[#]([0-9]|[a-f]){6}$/m',
    'ecl' => '/^(amb|blu|brn|gry|grn|hzl|oth)$/m',
    'pid' => '/^([0-9]{9})$/m'
];

print_r('Part2:' . countValidPassports($passports, $validFields, $validationRules) . PHP_EOL);
