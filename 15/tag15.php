#!/usr/bin/env php
<?php declare(strict_types=1);
/*
 * use with ./tag15.php [int,int,...] [cap]
 * example: ./tag15.php 1,2,3 2000
 */

$bits = explode(',', $argv[1]);
$cap  = $argv[2];
$i    = 0;
$time = [];
$say  = null;

foreach ($bits as $bit) {
    $i++;
    $time[$bit] = $i;
    $say        = 0;
}
for (; $i < $cap - 1;) {
    $i++;
    if (isset($time[$say])) {
        $last = $time[$say];
    } else {
        $last = $i;
    }
    $time[$say] = $i;
    $say        = $i - $last;
}
echo $say . PHP_EOL;
