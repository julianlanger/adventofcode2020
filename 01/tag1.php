<?php declare(strict_types=1);

$input = file(__DIR__ . '/input');

foreach ($input as $item => $value) {
    $input[$item] = (int)$value;
}

//Part1:
function find2NumbersMatching(array $input): int
{
    foreach ($input as $line => $number) {
        foreach ($input as $item => $value) {
            if ($item !== $line && ($number + $value === 2020)) {
                return $number * $value;
            }
        }
    }
}

print_r('Part1: ' . find2NumbersMatching($input) . PHP_EOL);

//Part2:

function find3NumbersMatching(array $input): int
{
    foreach ($input as $item1 => $value1) {
        foreach ($input as $item2 => $value2) {
            foreach ($input as $item3 => $value3) {
                if ($item1 !== $item2 && $item1 !== $item3 && ($value1 + $value2 + $value3 === 2020)) {
                    return $value1 * $value2 * $value3;
                }
            }
        }
    }
}

print_r('Part2: ' . find3NumbersMatching($input) . PHP_EOL);
