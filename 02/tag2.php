<?php declare(strict_types=1);

$input = explode("\n", file_get_contents('day2Input'));

foreach ($input as $index => $line) {
    $input[$index]    = explode(' ', $line);
    $input[$index][0] = explode('-', $input[$index][0]);
    $input[$index][1] = str_replace(':', '', $input[$index][1]);
    $input[$index][2] = str_split($input[$index][2]);
}

//part1
$countValid = 0;
foreach ($input as $ruleset) {
    $countChar = 0;
    foreach ($ruleset[2] as $item) {
        if ($item === $ruleset[1]) {
            $countChar++;
        }
    }
    if ($countChar >= $ruleset[0][0] && $countChar <= $ruleset[0][1]) {
        $countValid++;
        $countChar = 0;
    }
}

print_r('part1: ' . $countValid . PHP_EOL);

//part2

$countValid = 0;
foreach ($input as $ruleset) {
    if ($ruleset[2][$ruleset[0][0] - 1] === $ruleset[1]) {
        if (!($ruleset[2][$ruleset[0][1] - 1] === $ruleset[1])) {
            $countValid++;
        }
    } elseif ($ruleset[2][$ruleset[0][1] - 1] === $ruleset[1]) {
        if (!($ruleset[2][$ruleset[0][0] - 1] === $ruleset[1])) {
            $countValid++;
        }
    }
}

print_r('part2: ' . $countValid . PHP_EOL);
