<?php declare(strict_types=1);

$input = file(__DIR__ . '/input');

foreach ($input as $index => $data) {
    $input[$index] = [
        'row' => 0,
        'column' => 0,
        'seatID' => 0
        ];
    
    [$rows, $columns] = str_split($data, 7);
    
    $rows = str_replace('F', '0', $rows);
    $rows = str_replace('B', '1', $rows);
    
    $columns = str_replace('L', '0', $columns);
    $columns = str_replace('R', '1', $columns);
    
    $input[$index]['row']    = base_convert($rows, 2, 10);
    $input[$index]['column'] = base_convert($columns, 2, 10);
    $input[$index]['seatID'] = $input[$index]['row'] * 8 + $input[$index]['column'];
}

function getSeatIds(array $input): array
{
    $seatIDs = [];
    foreach ($input as $data) {
        $seatIDs[] = $data['seatID'];
    }
    
    return $seatIDs;
}

//part1

print_r('Part1 max seatID: ' . max(getSeatIds($input)) . PHP_EOL);

//part2

function getMySeat(array $input): ?int
{
    $seats          = getSeatIds($input);
    $availableSeats = [];
    
    foreach ($seats as $seat) {
        if (!in_array($seat + 1, $seats) && in_array($seat + 2, $seats)) {
            $availableSeats[] = $seat + 1;
        }
    }
    
    if (count($availableSeats) === 1) {
        return $availableSeats[0];
    }
    
    return null;
}

print_r('Part2 My Seat is: ' . getMySeat($input) . PHP_EOL);
