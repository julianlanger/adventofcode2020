<?php declare(strict_types=1);

$input = file(__DIR__ . '/input');

foreach ($input as $index => $number) {
    $input[$index] = (int)$number;
}

asort($input);
array_unshift($input, 0);
array_push($input, max($input) + 3);

// reindex:
$input = array_values($input);

// Part 1:
function findNextAdapterMin(array $input, int $joltage): int
{
    $adapters = [];
    
    foreach ($input as $index => $number) {
        if ($joltage >= $number - 3 && $joltage <= $number) {
            $adapters[$index] = $number;
        }
    }
        
    return min($adapters);
}

function findDifferences(array &$input): array
{
    $differences    = ['1 jolt' => 0, '3 jolt' => 0];
    $buildInAdapter = max($input);
    
    while (reset($input) < $buildInAdapter && end($input)) {
        reset($input);
        $adapter = current($input);
        array_shift($input);
        $nextAdapter = findNextAdapterMin($input, $adapter);
        if ($nextAdapter === $adapter + 1) {
            $differences['1 jolt']++;
        } elseif ($nextAdapter === $adapter + 3) {
            $differences['3 jolt']++;
        }
    }
    
    return $differences;
}

function getMultipliedDifferences($input): int
{
    $differences = findDifferences($input);
    return reset($differences) * end($differences);
}


print_r('Part1: ' . getMultipliedDifferences($input) . PHP_EOL);

// Part 2:

function combination(array $input, array &$memo = []): int
{
    $key = join(',', $input);
    if (isset($memo[$key])) {
        return $memo[$key];
    }
    
    $result = 1;
    for ($i = 1; $i < count($input) - 1; $i++) {
        if ($input[$i + 1] - $input[$i - 1] <= 3) {
            $inputTemp = array_merge([$input[$i - 1]], array_slice($input, $i + 1));
            $result   += combination($inputTemp, $memo);
        }
    }
    $memo[$key] = $result;
    return $result;
}

print_r(combination($input) . PHP_EOL);
