<?php declare(strict_types=1);

class Programm
{
    /** @var Block[] $blocks */
    private array $blocks = [];
    
    private array $result;

    public function __construct(array $input)
    {
        $this->convertInput($input);
        $this->result = array_fill(0, 35, 0);
    }
    
    public function run(): array
    {
        $addresses = ['part1' => [], 'part2' => []];
        foreach ($this->blocks as $block) {
            $execute = $block->execute();
            foreach ($execute['part1'] as $address => $value) {
                $addresses['part1'][$address] = $value;
            }
            foreach ($execute['part2'] as $address => $value) {
                $addresses['part2'][$address] = $value;
            }
        }
        
        $result = ['part1' => 0, 'part2' => 0];
        foreach ($addresses as $part => $addressesPart) {
            foreach ($addressesPart as $address) {
                $result[$part] += (int)$address;
            }
        }
        return $result;
    }
    
    private function convertInput(array $input): void
    {
        $line     = 0;
        $commands = [];
        while ($line < count($input)) {
            $commandsTmp = $input[$line];
            $commandsTmp = str_replace(PHP_EOL, '', $commandsTmp);
            $commandsTmp = explode(' = ', $commandsTmp);
            $commands[]  = $commandsTmp;
            $line++;
        }
        $line = 0;
        while ($line < count($commands)) {
            if ($commands[$line][0] === 'mask') {
                $mask = $commands[$line][1];
                $line++;
                $storage = [];
                while ($line < count($commands) && $commands[$line][0] !== 'mask') {
                    $address   = (int)str_replace(['mem[', ']'], '', $commands[$line][0]);
                    $value     = (int)$commands[$line][1];
                    $storage[] = ['address' => $address, 'value' => $value];
                    $line++;
                }
                $this->blocks[] = new Block($mask, $storage);
            }
        }
    }
}

class Block
{
    private string $mask;
    
    private array $storage;
    
    private array $addresses = [];
    
    public function __construct(string $mask, array $storage)
    {

        $this->mask      = $mask;
        $this->storage   = $storage;
        $this->addresses = ['part1' => [], 'part2' => []];
    }
    
    public function execute(): array
    {
        foreach ($this->storage as $storage) {
            $addressValue                                  = $this->applyMaskPart1($storage['value']);
            $this->addresses['part1'][$storage['address']] = $addressValue;
            $addresses                                     = $this->applyMaskPart2($storage['address']);
            foreach ($addresses as $address) {
                $this->addresses['part2'][$address] = $storage['value'];
            }
            [$storage['address']] = $addressValue;
        }
        
        return $this->addresses;
    }
    
    private function applyMaskPart1(int $value): string
    {
        $addressValue = str_repeat('0', strlen($this->mask));
        $value        = base_convert($value, 10, 2);
        $valueIndex   = strlen($value) - 1;
        for ($x = strlen($this->mask) - 1; $x >= 0; $x--) {
            if ($this->mask[$x] === '1') {
                $addressValue[$x] = '1';
            } elseif ($this->mask[$x] === '0') {
                $addressValue[$x] = '0';
            } elseif ($valueIndex >= 0) {
                $addressValue[$x] = $value[$valueIndex];
            }
            $valueIndex--;
        }
        
        return base_convert($addressValue, 2, 10);
    }
    
    private function applyMaskPart2(int $address): array
    {
        return $this->getAllAddresses($this->applyMaskToAddress($address));
    }
    
    private function applyMaskToAddress(int $address): string
    {
        $address = str_pad(base_convert($address, 10, 2), strlen($this->mask), '0', STR_PAD_LEFT);
        for ($x = 0; $x < strlen($address); $x++) {
            if ($this->mask[$x] === '1') {
                $address[$x] = 1;
            } elseif ($this->mask[$x] === 'X') {
                $address[$x] = 'X';
            }
        }
        return $address;
    }
    
    private function convertAdressesToIntArr(array $addresses): array
    {
        $return = [];
        foreach ($addresses as $address) {
            $return[] = (int)base_convert($address, 2, 10);
        }
        
        return $return;
    }
    
    private function getAllAddresses(string $address): array
    {
        $addresses = [''];
        for ($x = 0; $x < strlen($address); $x++) {
            if ($address[$x] === '0' || $address[$x] === '1') {
                foreach ($addresses as &$add) {
                    $add .= $address[$x];
                }
            }
            if ($address[$x] === 'X') {
                $addr = [];
                foreach ($addresses as &$add) {
                    $addr[] = $add . '0';
                    $addr[] = $add . '1';
                }
                $addresses = $addr;
            }
        }
        
        return $this->convertAdressesToIntArr($addresses);
    }
}


$input = file(__DIR__ . '/input');

$run = (new Programm($input))->run();
print_r('Part1: ' . $run['part1'] . PHP_EOL);

print_r('Part2: ' . $run['part2'] . PHP_EOL);
