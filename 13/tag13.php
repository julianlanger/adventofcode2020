<?php declare(strict_types=1);

$input = file(__DIR__ . '/input');
define('TIME_NOW', (int)$input[0]);
$input  = explode(',', $input[1]);
$busses = [];
foreach ($input as $index => $bus) {
    if ($bus !== 'x') {
        $busses[$index] = (int)$bus;
    }
}

// Part 1:
function getNextDeparture(array $busses): array
{
    $nextBusses = [];
    foreach ($busses as $bus) {
        $nextBusses[$bus] = $bus * (int)(TIME_NOW / $bus) + $bus;
    }
    
    asort($nextBusses);
    
    return ['id' => array_keys($nextBusses)[0], 'departure' => reset($nextBusses)];
}

$nextBus    = getNextDeparture($busses);
$timeToWait = $nextBus['departure'] - TIME_NOW;
print_r('Part 1: ' . $nextBus['id'] * $timeToWait . PHP_EOL);

// Part 2:

function getTimestampPart2(array $busses): int
{
    $number        = 1;
    $currentFactor = 1;
    foreach ($busses as $index => $bus) {
        while ((($number + $index) % $bus) != 0) {
            $number += $currentFactor;
        }
        $currentFactor *= $bus;
    }
    return $number;
}

print_r('Part 2: ' . getTimestampPart2($busses) . PHP_EOL);
