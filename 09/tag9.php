<?php declare(strict_types=1);

$input     = file(__DIR__ . '/input');
$startLine = 25;

foreach ($input as $index => $number) {
    $input[$index] = (int)$number;
}

function getPreambleArray(array $input, int $currentLine, int $startLine): array
{
    $preambleRange = [$currentLine - $startLine, $currentLine - 1];
    $rangeArray    = [];
    for ($x = $preambleRange[0]; $x <= $preambleRange[1]; $x++) {
        $rangeArray[] = $input[$x];
    }
    
    return $rangeArray;
}

/**
 * x + y = z
 * Mathematic rules:
 *
 * 1: z = 0 <=> x = 0 && y = 0
 * 2: x = 0 || y = 0 <=> y = z || x = z
 *
 * general:
 * z - x = y &&  z - y = x
 *
 *
 *
 * @param array $rangeArray
 * @param int   $checkNumber
 *
 * @return bool
 */
function checkNumber(array $rangeArray, int $checkNumber): bool
{
    //first check if there is ANY 0 in $rangeArray:
    if (in_array(0, $rangeArray)) {
        //If $checkNumber is 0, there must be two zeros in $rangeArray (rule 1):
        if ($checkNumber === 0) {
            //count zeros in $rangeArray:
            $countZero = 0;
            foreach ($rangeArray as $number) {
                if ($countZero >= 2) {
                    break;
                }
                if ($number === 0) {
                    $countZero++;
                }
            }

            if ($countZero >= 2) {
                return true;
            }
            // if $checkNumber exists in $rangeArray, there must be a 0 in $rangeArray (rule 2):
        } elseif (in_array($checkNumber, $rangeArray)) {
            return true;
        }
    } else {
        // $checkNumber - $rangeNumber must exist in $rangeArray (general rule):
        foreach ($rangeArray as $rangeNumber) {
            if (in_array($checkNumber - $rangeNumber, $rangeArray)) {
                return true;
            }
        }
    }
    return false;
}

//Part1:

function checkInput(array $input, int $startLine): ?array
{
    $count = $startLine;
    while ($count < count($input)) {
        if (checkNumber(getPreambleArray($input, $count, $startLine), $input[$count]) === false) {
            return [$input[$count], $count];
        }
        $count++;
    }

    return null;
}

print_r('Part1: ' . checkInput($input, $startLine)[0] . PHP_EOL);

//part2:

function getMinMax(array $input, int $startLine): int
{
    [$failedNumber, $failedLine] = checkInput($input, $startLine);
    $linesBefore                 = array_chunk($input, $failedLine)[0];
    $currentPart                 = [$linesBefore[0]];
    $count                       = 1;
    while ($count < count($linesBefore)) {
        $currentPart[] = $linesBefore[$count];
        while (array_sum($currentPart) > $failedNumber) {
            array_shift($currentPart);
        }
        if (array_sum($currentPart) === $failedNumber) {
            return min($currentPart) + max($currentPart);
        }
        $count++;
    }
    return 0;
}

print_r('Part2: ' . getMinMax($input, $startLine) . PHP_EOL);
