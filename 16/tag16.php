<?php declare(strict_types=1);

$rules         = file(__DIR__ . '/rules');
$myTicket      = file(__DIR__ . '/myTicket');
$nearbyTickets = file(__DIR__ . '/nearbyTickets');

function getRules(array $rules): array
{
    $result = [];
    foreach ($rules as $rule) {
        preg_match_all('/^[a-z ]+/', $rule, $matches);
        $name          = $matches[0][0];
        $validNumbers  = getValidNumbers([$rule]);
        $result[$name] = $validNumbers;
    }
    
    return $result;
}

function getValidNumbers(array $rules): array
{
    $result = [];
    foreach ($rules as $rule) {
        preg_match_all('/[0-9\-]+/', $rule, $matches);
        foreach ($matches[0] as $match) {
            $range = explode('-', $match);
            $a     = (int)$range[0];
            $b     = (int)$range[1];
            for ($i = $a; $i <= $b; $i++) {
                $result[] = $i;
            }
        }
    }
    return array_unique($result);
}

function ticketToIntArray(array $tickets): array
{
    $return = [];
    foreach ($tickets as $ticket) {
        $numberArr = explode(',', $ticket);
        foreach ($numberArr as $index => $number) {
            $numberArr[$index] = (int)$number;
        }
        $return[] = $numberArr;
    }
    
    return $return;
}

function getValidTicketsAndErrorRate(array $rules, array $nearbyTickets): array
{
    $validNumbers = getValidNumbers($rules);
    $tickets      = ticketToIntArray($nearbyTickets);
    $errorRate    = 0;
    $validTickets = [];
    
    foreach ($tickets as $ticket) {
        $counter = 0;
        foreach ($ticket as $number) {
            if (!in_array($number, $validNumbers)) {
                $counter += $number;
            }
            if ($number === 0) {
                continue 2;
            }
        }
        $errorRate += $counter;
        if ($counter === 0) {
            $validTickets[] = $ticket;
        }
    }
    
    return ['validTickets' => $validTickets, 'errorRate' => $errorRate];
}

function getFieldNames(array $validTickets, array $rules): array
{
    $sortArray = [];
    $return    = [];
    foreach (array_keys($rules) as $name) {
        for ($x = 0; $x < count(reset($validTickets)); $x++) {
            foreach ($validTickets as $ticket) {
                if (!in_array($ticket[$x], $rules[$name])) {
                    continue 2;
                }
            }
            $sortArray[$name][] = $x;
        }
    }
    
    uasort($sortArray, fn($a, $b) => count($a) - count($b));
    
    while (count($sortArray) > 0) {
        $keyFirstElement          = array_keys($sortArray)[0];
        $value                    = reset($sortArray);
        $value                    = reset($value);
        $return[$keyFirstElement] = $value;
        foreach ($sortArray as $name => $fields) {
            if (in_array($value, $fields)) {
                $key = array_search($value, $fields);
                unset($sortArray[$name][$key]);
            }
            if (count($sortArray[$name]) === 0) {
                unset($sortArray[$name]);
            }
        }
    }
    
    return $return;
}

function getDepartureFields(array $fields): array
{
    $departureFields = [];
    foreach ($fields as $name => $field) {
        if (strpos($name, 'departure') === 0) {
            $departureFields[] = $field;
        }
    }
    
    return $departureFields;
}

function multiplyDeparture(array $myTicket, array $departureFields): int
{
    $result = 1;
    foreach ($departureFields as $field) {
        $result *= $myTicket[$field];
    }
    
    return $result;
}

$validAndRate = getValidTicketsAndErrorRate($rules, $nearbyTickets);
$validTickets = $validAndRate['validTickets'];
$errorRate    = $validAndRate['errorRate'];

print_r('Part 1: ' . $errorRate . PHP_EOL);


$departureFields = getDepartureFields(getFieldNames($validTickets, getRules($rules)));
$myTicket        = ticketToIntArray($myTicket)[0];

print_r('Part 2: ' . multiplyDeparture($myTicket, $departureFields) . PHP_EOL);
