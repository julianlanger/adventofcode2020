<?php declare(strict_types=1);

define('FACING_START', 'E');
define('WAYPOINT_START', ['N' => 1, 'E' => 10, 'S' => 0, 'W' => 0]);

$input = file(__DIR__ . '/input');

foreach ($input as $index => $command) {
    $direction     = (string)$command[0];
    $amount        = (int)str_replace($direction, '', $command);
    $input[$index] = ['direction' => $direction, 'amount' => $amount];
}

function stepShip(array $input): array
{
    $returnArr = ['N' => 0, 'E' => 0, 'S' => 0, 'W' => 0];
    $facing    = FACING_START;
    foreach ($input as $index => $navigation) {
        $direction = $navigation['direction'];
        $amount    = $navigation['amount'];
        if (in_array($direction, ['L', 'R'])) {
            $facing = turnShip($facing, $direction, $amount);
        } elseif ($direction === 'F') {
            $returnArr = move($returnArr, $facing, $amount);
        } else {
            $returnArr = move($returnArr, $direction, $amount);
        }
    }
    
    return $returnArr;
}

function turnShip(string $oldFacing, string $direction, int $amount): string
{
    $turnArray    = ['N', 'E', 'S', 'W'];
    $facingKey    = array_search($oldFacing, $turnArray);
    $amount       = $amount / 90;
    $newFacingKey = $direction === 'L' ? $facingKey - $amount : $facingKey + $amount;
    $newFacingKey = isset($turnArray[$newFacingKey]) ?
        $newFacingKey :
        (
        $direction === 'L' ?
            $newFacingKey + count($turnArray) :
            $newFacingKey - count($turnArray)
        );
    return $turnArray[$newFacingKey];
}

function move(array $currentPosition, string $direction, int $amount): array
{
    if (in_array($direction, ['N', 'E', 'S', 'W'])) {
        $currentPosition[$direction] += $amount;
    }

    return $currentPosition;
}

function calculateResult(array $result): int
{
    $north = $result['N'];
    $east  = $result['E'];
    $south = $result['S'];
    $west  = $result['W'];
    
    $northSouth = $north >= $south ? $north - $south : $south - $north;
    $eastWest   = $east >= $west ? $east - $west : $west - $east;
    
    return $northSouth + $eastWest;
}

print_r('Part 1: ' . calculateResult(stepShip($input)) . PHP_EOL);

// part 2:

function moveShipToWaypoint(array $waypoint, array $ship, int $moveAmount): array
{
    foreach ($waypoint as $direction => $amount) {
        $ship[$direction] += $amount * $moveAmount;
    }
    
    return $ship;
}

function flipWaypoint(array $waypoint, string $direction, int $amount): array
{
    $newWaypoint = $waypoint;
    $turnArray   = ['N', 'E', 'S', 'W'];
    $amount      = $amount / 90;
    $amount      = $direction === 'L' ? $amount * -1 : $amount;
    
    foreach (array_keys($turnArray) as $oldDirectionKey) {
        $newDirectionKey                           = $oldDirectionKey + $amount;
        $newDirectionKey                           = isset($turnArray[$newDirectionKey]) ?
            $newDirectionKey :
            (
            $direction === 'L' ?
                $newDirectionKey + count($turnArray) :
                $newDirectionKey - count($turnArray)
            );
        $newWaypoint[$turnArray[$newDirectionKey]] = $waypoint[$turnArray[$oldDirectionKey]];
    }
    
    return $newWaypoint;
}

function moveShipAndWayPoint(array $input): array
{
    $ship     = ['N' => 0, 'E' => 0, 'S' => 0, 'W' => 0];
    $waypoint = WAYPOINT_START;
    foreach ($input as $index => $navigation) {
        $direction = $navigation['direction'];
        $amount    = $navigation['amount'];
        if (in_array($direction, ['L', 'R'])) {
            $waypoint = flipWaypoint($waypoint, $direction, $amount);
        } elseif ($direction === 'F') {
            $ship = moveShipToWaypoint($waypoint, $ship, $amount);
        } else {
            $waypoint = move($waypoint, $direction, $amount);
        }
    }
    return $ship;
}

print_r('Part 2: ' . calculateResult(moveShipAndWayPoint($input)) . PHP_EOL);
