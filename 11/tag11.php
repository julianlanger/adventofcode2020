<?php declare(strict_types=1);

define('SEAT_OCCUPIED', '#');
define('SEAT_EMPTY', 'L');
define('FLOOR', '.');

$input = file(__DIR__ . '/input');

foreach ($input as $row => $seats) {
    $input[$row] = str_split($seats);
}

function run(array $input, int $maxOccupied = 4, int $part = 1): array
{
    $row    = 0;
    $output = $input;
    while ($row < count($input)) {
        foreach ($input[$row] as $col => $type) {
            if (isSeat($type)) {
                $occupiedSeats = $part === 1 ?
                    getAdjacentOccupiedSeatsPart1($input, $row, $col) :
                    getAdjacentOccupiedSeatsPart2($input, $row, $col);
                if ($type === SEAT_EMPTY && $occupiedSeats === 0) {
                    $type = SEAT_OCCUPIED;
                } elseif ($type === SEAT_OCCUPIED && $occupiedSeats >= $maxOccupied) {
                    $type = SEAT_EMPTY;
                }
                $output[$row][$col] = $type;
            }
        }
        $row++;
    }
    
    return $output;
}

function isSeat(string $type): bool
{
    return $type !== FLOOR;
}
function getAdjacentOccupiedSeatsPart1(array $input, int $row, int $col): int
{
    $occupiedSeats = 0;
    if (isset($input[$row - 1][$col - 1]) && $input[$row - 1][$col - 1] === SEAT_OCCUPIED) {
        $occupiedSeats++;
    }
    if (isset($input[$row - 1][$col]) && $input[$row - 1][$col] === SEAT_OCCUPIED) {
        $occupiedSeats++;
    }
    if (isset($input[$row - 1][$col + 1]) && $input[$row - 1][$col + 1] === SEAT_OCCUPIED) {
        $occupiedSeats++;
    }
    if (isset($input[$row][$col - 1]) && $input[$row][$col - 1] === SEAT_OCCUPIED) {
        $occupiedSeats++;
    }
    if (isset($input[$row][$col + 1]) && $input[$row][$col + 1] === SEAT_OCCUPIED) {
        $occupiedSeats++;
    }
    if (isset($input[$row + 1][$col - 1]) && $input[$row + 1][$col - 1] === SEAT_OCCUPIED) {
        $occupiedSeats++;
    }
    if (isset($input[$row + 1][$col]) && $input[$row + 1][$col] === SEAT_OCCUPIED) {
        $occupiedSeats++;
    }
    if (isset($input[$row + 1][$col + 1]) && $input[$row + 1][$col + 1] === SEAT_OCCUPIED) {
        $occupiedSeats++;
    }
    
    return $occupiedSeats;
}

function runUntilEnd(array $input, int $maxOccupied = 4, int $part = 1, array &$runs = [], array &$run = []): array
{
    if (!$runs && !$run) {
        $run = run($input, $maxOccupied, $part);
    }
    $key = serialize($run);
    
    if (!isset($runs[$key])) {
        $run        = run($run, $maxOccupied, $part);
        $runs[$key] = $run;
        runUntilEnd($input, $maxOccupied, $part, $runs, $run);
    }
    
    return $run;
}

function countOccupiedSeats(array $input): int
{
    $count = 0;
    foreach ($input as $row) {
        foreach ($row as $type) {
            if ($type === SEAT_OCCUPIED) {
                $count++;
            }
        }
    }
    
    return $count;
}

$occupiedSeats = countOccupiedSeats(runUntilEnd($input));

print_r('Part 1: ' . $occupiedSeats . PHP_EOL);

// part 2:

function getAdjacentOccupiedSeatsPart2(array $input, int $rowInput, int $colInput): int
{
    $occupiedSeats = 0;
    
    $y          = $rowInput;
    $x          = $colInput;
    $z          = 1;
    $directions = [1,1,1,1,1,1,1,1];
    
    while (array_unique($directions) !== [0]) {
        $grid = [
            [$y - $z, $x - $z], [$y - $z, $x], [$y - $z, $x + $z],
            [$y, $x - $z], [$y, $x + $z],
            [$y + $z, $x - $z], [$y + $z, $x], [$y + $z, $x + $z]
        ];
        foreach ($grid as $index => $element) {
            if ($element && $directions[$index] === 1) {
                [$row, $col] = $element;
                if (isset($input[$row][$col])) {
                    if (($seat = $input[$row][$col]) !== FLOOR) {
                        if ($seat === SEAT_OCCUPIED) {
                            $occupiedSeats++;
                        }
                        $directions[$index] = 0;
                    }
                } else {
                    $directions[$index] = 0;
                }
            }
        }
        $z++;
    }
    
    return $occupiedSeats;
}

$occupiedSeats = countOccupiedSeats(runUntilEnd($input, 5, 2));

print_r('Part 2: ' . $occupiedSeats . PHP_EOL);
