<?php declare(strict_types=1);

$input = explode("\n", file_get_contents(__DIR__ . '/input'));

foreach ($input as $index => $line) {
    $input[$index] = [
        'pattern' => str_split($line),
        'value' => str_split($line)
        ];
}

function walk(int $down, int $right, $input): int
{
    $currentIndex = 0;
    $currentLine  = 0;
    $trees        = 0;
    while ($currentLine < count($input)) {
        if (!isset($input[$currentLine]['value'][$currentIndex])) {
            $input[$currentLine]['value'] =
                array_merge_recursive($input[$currentLine]['value'], $input[$currentLine]['pattern']);
            continue;
        }
        if ($input[$currentLine]['value'][$currentIndex] === '#') {
            $trees++;
        }
        $currentLine  += $down;
        $currentIndex += $right;
    }
    
    return $trees;
}

print_r('Part1: ' . walk(1, 3, $input) . PHP_EOL);

//part2:

$result = walk(1, 1, $input) * walk(1, 3, $input) * walk(1, 5, $input) * walk(1, 7, $input) * walk(2, 1, $input);

print_r('Part2: ' . $result . PHP_EOL);
